package sandbox;

import java.util.Scanner;

class BinarySearchTreeNode {

    BinarySearchTreeNode left, right;
    int value;

    /* Constructor */
    public BinarySearchTreeNode(int val) {
        left = null;
        right = null;
        value = val;
    }

    /* Function to get left node */
    public BinarySearchTreeNode getLeft() {
        return left;
    }

    /* Function to get right node */
    public BinarySearchTreeNode getRight() {
        return right;
    }

    /* Function to set data to node */
    public void setData(int v) {
        value = v;
    }

    /* Function to get data from node */
    public int getValue() {
        return value;
    }
}

class BinarySearchTree {
    public BinarySearchTreeNode root;

    /* Constructor */
    public BinarySearchTree() {
        root = null;
    }

    /* Functions to insert data */
    public void insert(int data) {
        root = insert(root, data);
    }

    public void remove(int data) {
        root = remove(root, data);
    }

    private BinarySearchTreeNode remove(BinarySearchTreeNode node, int data) {
        if (node == null) {
            return null;
        }
        if (data < node.value) {
            node.left = remove(node.left, data);
        } else if (data > node.value) {
            node.right = remove(node.right, data);
        } else if (node.left != null && node.right != null) {
            BinarySearchTreeNode nodeMin = getMin(node.right);
            node.value = nodeMin.value;
            node.left = removeMin(node.right);
        } else {
            node = node.left == null ? node.right : node.left;
        }

        return node;
    }

    private BinarySearchTreeNode removeMin(BinarySearchTreeNode node) {
        if (node == null) {
            return null;
        } else if (node.left != null) {
            node.left = removeMin(node.left);
            return node;
        } else {
            return node.right;
        }
    }

    private BinarySearchTreeNode getMin(BinarySearchTreeNode node) {
        BinarySearchTreeNode parent = null;
        while (node != null) {
            parent = node;
            node = node.left;
        }
        return parent;
    }

    /* Function to insert data recursively */
    private BinarySearchTreeNode insert(BinarySearchTreeNode node, int value) {
        if (node == null)
            node = new BinarySearchTreeNode(value);
        else {
            if (value <= node.getValue())
                node.left = insert(node.left, value);
            else
                node.right = insert(node.right, value);
        }
        return node;
    }


    /* Inorder traversal */
    public void inorder(BinarySearchTreeNode node) {
        if (node != null) {
            inorder(node.getLeft());
            System.out.print(node.getValue() + " ");
            inorder(node.getRight());
        }
    }

    /* Postorder traversal */
    public void postorder(BinarySearchTreeNode node) {
        if (node != null) {
            postorder(node.getLeft());
            postorder(node.getRight());
            System.out.print(node.getValue() + " ");
        }
    }
}

class BinarySearchTreeTest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        BinarySearchTree bst = new BinarySearchTree();
        System.out.println("Binary Search Tree Test\n");

	        /* Insert elements in tree */
        bst.insert(6);
        bst.insert(10);
        bst.insert(5);
        bst.insert(2);
        bst.insert(3);
        bst.insert(4);
        bst.insert(11);

        BinarySearchTreeNode r = bst.root;

	        /* Display tree in both postorder and inorder */
        System.out.print("\nPost order : ");
        bst.postorder(r);
        System.out.print("\nIn order : ");
        bst.inorder(r);

    }
}