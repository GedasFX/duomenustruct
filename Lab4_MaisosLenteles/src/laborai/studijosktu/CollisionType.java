package laborai.studijosktu;

public enum CollisionType {
    CHAIN, REHASH
}
