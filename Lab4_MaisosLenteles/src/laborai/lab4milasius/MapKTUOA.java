package laborai.lab4milasius;

import laborai.studijosktu.HashType;
import laborai.studijosktu.MapADT;

import java.util.Arrays;
import java.util.Objects;

// Dviguba maisa
public class MapKTUOA<K, V> implements MapADT<K, V> {
    public static final int DEFAULT_INITIAL_CAPACITY = 16;
    public static final float DEFAULT_LOAD_FACTOR = 0.75f;
    public static final HashType DEFAULT_HASH_TYPE = HashType.DIVISION;

    protected final HashType hashType;
    protected float loadFactor;

    protected Entry<K, V>[] table;
    protected int size;

    protected int lastUpdatedIndex;
    protected int rehashesCount;

    public MapKTUOA() {
        this(DEFAULT_HASH_TYPE);
    }

    public MapKTUOA(HashType ht) {
        this(DEFAULT_INITIAL_CAPACITY, ht);
    }

    public MapKTUOA(int initialCapacity, HashType ht) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR, ht);
    }

    public MapKTUOA(float loadFactor, HashType ht) {
        this(DEFAULT_INITIAL_CAPACITY, loadFactor, ht);
    }

    public MapKTUOA(int initialCapacity, float loadFactor, HashType ht) {
        if (initialCapacity <= 0) {
            throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);
        }

        if ((loadFactor <= 0.0) || (loadFactor > 1.0)) {
            throw new IllegalArgumentException("Illegal load factor: " + loadFactor);
        }

        this.table = new Entry[initialCapacity];
        this.loadFactor = loadFactor;
        this.hashType = ht;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        Arrays.fill(table, null);
        size = 0;
    }

    // Tikslas?
    @Override
    public String[][] toArray() {
        String[][] result = new String[table.length][];
        int count = 0;
        for (Entry<K, V> n : table) {
            String[] list = new String[1];
            list[0] = n.toString();
            result[count] = list;
            count++;
        }
        return result;
    }

    @Override
    public V put(K key, V value) {
        if (key == null || value == null) {
            throw new IllegalArgumentException("Key or value is null in put(Key key, Value value)");
        }
        int index = findPosition(key);
        Entry entry = new Entry<>(key, value);
        if (index == -1) {
            rehash(entry);
            return put(key, value);
        }
        if (table[index] == null) {
            table[index] = entry;
            size++;

            if (size > table.length * loadFactor) {
                rehash(table[index]);
            }
        } else {
            table[index].value = value;
            lastUpdatedIndex = index;
        }

        return value;
    }

    public int getEmptyEntriesCount() {
        int empty = 0;
        for (int i = 0; i < table.length; i++) {
            if (table[i] == null)
                empty++;
        }
        return empty;
    }

    /**
     * Patikrina ar elementas egzistuoja HashMap'e
     *
     * @param value Elementas kurį reikia tikrinti
     * @return <tt>true</tt>, jei elementas egzistuoja HashMap'e
     */
    public boolean containsValue(V value) {
        if (size > 0) {
            for (Entry<K, V> entry : table) {
                if (entry == null)
                    continue;
                if (entry.value.equals(value))
                    return true;
            }
        }
        return false;
    }

    public V putIfAbscent(K key, V value) {
        int index = findPosition(key);
        return index == -1 || table[index] == null ? put(key, value) : null;
    }

    public int getNumberOfCollisions() {
        int col = 0;
        for (int i = 0; i < table.length; i++) {
            if (table[i] != null)
                col += hash(table[i].key, hashType) == i ? 0 : 1;
        }
        return col;
    }

    @Override
    public V get(K key) {
        int index = findPosition(key);
        return table[index] != null ? table[index].value : null;
    }

    @Override
    public V remove(K key) {
        int index = findPosition(key);
        if (table[index] == null) {
            return null;
        }
        V ret = table[index].value;
        table[index] = null;
        size--;
        lastUpdatedIndex = index;
        return ret;
    }

    @Override
    public boolean contains(K key) {
        return get(key) != null;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Entry<K, V> e : table) {
            if (e != null) {
                result.append(e.toString()).append(System.lineSeparator());
            }
        }
        return result.toString();
    }

    private int hash(K key, HashType hashType) {
        int h = key.hashCode();
        switch (hashType) {
            case DIVISION:
                return Math.abs(h) % table.length;
            case MULTIPLICATION:
                double k = (Math.sqrt(5) - 1) / 2;
                return (int) (((k * Math.abs(h)) % 1) * table.length);
            case JCF7:
                h ^= (h >>> 20) ^ (h >>> 12);
                h = h ^ (h >>> 7) ^ (h >>> 4);
                return h & (table.length - 1);
            case JCF8:
                h = h ^ (h >>> 16);
                return h & (table.length - 1);
            default:
                return Math.abs(h) % table.length;
        }
    }

    private void rehash(Entry<K, V> entry) {
        MapKTUOA<K, V> map = new MapKTUOA<>(table.length * 2, loadFactor, hashType);
        for (Entry<K, V> e : table) {
            if (e != null)
                map.put(e.key, e.value);
        }
        table = map.table;
        rehashesCount++;
    }

    /**
     * Returns the index of the table, where the entry exists or should exist.
     * In case element exists - returns index of the element.
     * In case it doesnt - returns index of where the element should exist.
     * @param key Key to search. If it doesnt exist can be null.
     * @return Index of the entry.
     */
    private int findPosition(K key) {
        int index = hash(key, hashType);
        int oIndex = index; // Original index
        int hc = 0; // Hash count

        while (hc < table.length){
            if (table[index] == null || table[index].key.equals(key)) {
                return index;
            }
            index = (oIndex + ++hc * hash2(key)) % table.length;
        }
        return -1;
    }

    private int hash2(K key) {
        return 7 - Math.abs(key.hashCode() % 7);
    }

    protected class Entry<K, V> {
        protected K key;
        protected V value;

        protected Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return key + "= " + value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Entry<?, ?> entry = (Entry<?, ?>) o;
            return Objects.equals(key, entry.key);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key);
        }
    }
}
