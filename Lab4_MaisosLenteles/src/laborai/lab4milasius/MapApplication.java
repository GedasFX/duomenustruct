package laborai.lab4milasius;

import javafx.application.Application;
import javafx.stage.Stage;
import laborai.gui.fx.Lab4WindowFX;

import java.util.Locale;

public class MapApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Locale.setDefault(Locale.US);
        setUserAgentStylesheet(STYLESHEET_MODENA);
        Lab4WindowFX.createAndShowFXGUI(primaryStage);
    }
}
