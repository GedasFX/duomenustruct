package laborai.lab4milasius;

import laborai.demo.GreitaveikosTyrimas;
import laborai.gui.MyException;
import laborai.studijosktu.HashType;
import laborai.studijosktu.MapKTU;

import java.io.*;
import java.util.HashSet;

public class GreitaveikosTyrimas2 extends GreitaveikosTyrimas {
    private static final String[] TYRIMU_VARDAI = { "MapAdd", "MOAAdd", "SysAdd", "MapCon", "MOACon", "SysCon" };
    private static final int[] TIRIAMI_KIEKIAI = { 10000, 20000, 40000, 80000 };

    MapKTU<String, String> mapKTU = new MapKTU<>( 10, 0.25f, HashType.DIVISION);
    MapKTUOA<String, String> mapKTUOA = new MapKTUOA<>( 10, 0.25f, HashType.DIVISION);
    HashSet<String> hashSet = new HashSet<>();

    String[] words = generateWords("words.txt", 400000);

    @Override
    public void SisteminisTyrimas() throws InterruptedException {
        try {
            for (int k : TIRIAMI_KIEKIAI) {
                tk.startAfterPause();
                tk.start();

                for (int i = 0; i < k; i++) {
                    mapKTU.put(words[i], words[i]);
                }
                tk.finish(TYRIMU_VARDAI[0]);

                for (int i = 0; i < k; i++) {
                    mapKTUOA.put(words[i], words[i]);
                }
                tk.finish(TYRIMU_VARDAI[1]);

                for (int i = 0; i < k; i++) {
                    hashSet.add(words[i]);
                }
                tk.finish(TYRIMU_VARDAI[2]);

                for (int i = 0; i < k; i++) {
                    mapKTU.contains(words[i]);
                }
                tk.finish(TYRIMU_VARDAI[3]);

                for (int i = 0; i < k; i++) {
                    mapKTUOA.contains(words[i]);
                }
                tk.finish(TYRIMU_VARDAI[4]);

                for (int i = 0; i < k; i++) {
                    hashSet.contains(words[i]);
                }
                tk.finish(TYRIMU_VARDAI[5]);

                tk.seriesFinish();

            }
            tk.logResult(FINISH_COMMAND);
        } catch (MyException e) {
            tk.logResult(e.getMessage());
        }
    }

    private String[] generateWords(String path, int count) {
        String[] lines = new String[count];
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            for (int i = 0; i < count; i++) {
                lines[i] = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}
